FROM node:22-alpine@sha256:6e80991f69cc7722c561e5d14d5e72ab47c0d6b6cfb3ae50fb9cf9a7b30fdf97 AS ng-builder

WORKDIR /build
COPY . /build
RUN npm install
RUN npm install @angular/cli
RUN ./node_modules/.bin/ng build

FROM docker.io/library/nginx:1.27.3-alpine@sha256:41523187cf7d7a2f2677a80609d9caa14388bf5c1fbca9c410ba3de602aaaab4
COPY docker/nginx.conf /etc/nginx/nginx.conf
COPY --from=ng-builder /build/dist/cv /usr/share/nginx/html

RUN mkdir -p /var/cache/nginx && chown nginx:nginx /var/cache/nginx && \
    touch /run/nginx.pid && chown nginx:nginx /run/nginx.pid
USER nginx
CMD ["nginx"]

