import { Component } from '@angular/core';
import { SystemdLineComponent } from '../lib/systemd-line/systemd-line.component';

@Component({
  selector: 'app-about-me',
  standalone: true,
  imports: [SystemdLineComponent],
  templateUrl: './about-me.component.html',
  styleUrl: './about-me.component.scss'
})
export class AboutMeComponent {

}
