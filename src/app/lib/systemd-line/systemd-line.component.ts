import { Component, Injectable, Input, booleanAttribute } from '@angular/core';

@Component({
  selector: 'systemd-line',
  standalone: true,
  imports: [],
  templateUrl: './systemd-line.component.html',
  styleUrl: './systemd-line.component.scss',
})

@Injectable({providedIn: 'root'})

export class SystemdLineComponent {
  @Input({transform: booleanAttribute}) status: boolean = false;
  @Input({required: true}) message!: string;
}
