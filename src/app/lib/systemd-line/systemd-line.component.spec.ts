import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SystemdLineComponent } from './systemd-line.component';

describe('SystemdLineComponent', () => {
  let component: SystemdLineComponent;
  let fixture: ComponentFixture<SystemdLineComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SystemdLineComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SystemdLineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
