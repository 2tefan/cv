import { Component } from '@angular/core';
import { ProjectComponent } from '../lib/project/project.component';

@Component({
  selector: 'app-my-projects',
  standalone: true,
  imports: [ProjectComponent],
  templateUrl: './my-projects.component.html',
  styleUrl: './my-projects.component.scss'
})
export class MyProjectsComponent {

}
