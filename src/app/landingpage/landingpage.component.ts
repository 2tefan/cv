import { Component, HostListener } from '@angular/core';

@Component({
  selector: 'app-landingpage',
  templateUrl: './landingpage.component.html',
  styleUrls: ['./landingpage.component.scss', './glitch-animation.scss', './blocks.scss' ] ,
  standalone: true,
})

export class LandingpageComponent {
  scrollPercentage = 0;
  @HostListener('window:scroll', ['$event']) onScrollEvent() {
    let pos = document.documentElement.scrollTop;
    let calcHeight = document.documentElement.scrollHeight - document.documentElement.clientHeight;
    let scrollValue = Math.round(pos * 100 / calcHeight);
    this.scrollPercentage = scrollValue;
  }
}
